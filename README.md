# PowerDNS Authoritative Server

RPMs for pdns auth

## centos6

- Download pdns rpm file for **centos6** from here:
    - [pdns - centos6](https://gitlab.com/rpmbased/pdns-auth/-/jobs/artifacts/centos6/browse?job=run-build)

## centos7

- Download pdns rpm file for **centos7** from here:
    - [pdns - centos7](https://gitlab.com/rpmbased/pdns-auth/-/jobs/artifacts/centos7/browse?job=run-build)
